﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

//Agregados
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace BethanysPieShop.Models
{
    public class Order
    {
        public int OrderId { get; set; }

        public List<OrderDetail> OrderDetails { get; set; }

        [Required(ErrorMessage = "Por favor ingrese el primer nombre")]
        [Display(Name = "Primer Nombre")]
        [StringLength(50)]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Por favor ingrese el apellido")]
        [Display(Name = "Apellido")]
        [StringLength(50)]

        public string LastName { get; set; }

        [Required(ErrorMessage = "Por favor digite la dirección")]
        [StringLength(100)]
        [Display(Name = "Dirección Línea 1")]

        public string AddressLine1 { get; set; }

        [Display(Name = "Dirección Linea 2")]
        public string AddressLine2 { get; set; }

        [Required(ErrorMessage = "Por favor ingrese el código postal")]
        [Display(Name = "Codigo Postal")]
        [StringLength(10, MinimumLength = 4)]
        [DefaultValue("")]
        public string ZipCode { get; set; }

        [Required(ErrorMessage = "Por favor ingrese tu ciudad")]
        [StringLength(50)]

        public string City { get; set; }

        [StringLength(10)]
        public string State { get; set; }

        [Required(ErrorMessage = "Por favor ingrese tu país")]
        [StringLength(50)]
        public string Country { get; set; }

        [Required(ErrorMessage = "Por favor ingrese tu número telefónico")]
        [StringLength(25)]
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "'Número Teléfono")]
        public string PhoneNumber { get; set; }

        [Required]
        [StringLength(50)]
        [DataType(DataType.EmailAddress)]
        [RegularExpression(@"(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|""(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*"")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])",
            ErrorMessage = "The email address is not entered in a correct format")]
       public string Email { get; set; }

        [Column(TypeName = "decimal(18,4)")]
        public decimal OrderTotal { get; set; }

        public DateTime OrderPlaced { get; set; }

    }
}
