﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

//Agregados
using System.ComponentModel.DataAnnotations.Schema;

namespace BethanysPieShop.Models
{
    public class OrderDetail
    {
        public int OrderDetailId { get; set; }
        public int OrderId { get; set; }
        public int PieId { get; set; }
        [Column(TypeName = "decimal(18,4)")]
        public int Amount { get; set; }
        [Column(TypeName = "decimal(18,4)")]
        public decimal Price { get; set; }
        public Pie Pie { get; set; }
        public Order Order { get; set; }

    }
}
