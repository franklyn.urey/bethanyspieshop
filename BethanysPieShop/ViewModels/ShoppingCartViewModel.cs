﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
//Agregado
using BethanysPieShop.Models;

namespace BethanysPieShop.ViewModels
{
    public class ShoppingCartViewModel
    {
        public ShoppingCart ShoppingCart { get; set; }
        public decimal ShoppingCartTotal { get; set; }

    }
}
