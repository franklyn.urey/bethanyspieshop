﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
//Agregado.
using BethanysPieShop.Models;

    namespace BethanysPieShop.ViewModels
{
    public class PiesListModel
    {
        public IEnumerable<Pie> Pies { get; set; }
        public string CurrentCategory { get; set; }

    }
}
